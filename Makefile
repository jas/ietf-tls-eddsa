all: draft-josefsson-tls-eddsa.txt draft-josefsson-tls-eddsa2.txt

# To prevent xml2rfc from opening any windows
unexport DISPLAY

%.txt: %.xml
	xml2rfc $< -o $@ --text

%.html: %.xml
	xml2rfc $< -o $@ --html
