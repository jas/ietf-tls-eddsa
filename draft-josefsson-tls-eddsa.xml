<?xml version="1.0" encoding="US-ASCII"?>

<!DOCTYPE rfc SYSTEM "rfc2629.dtd" [
<!ENTITY rfc2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY rfc4492 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.4492.xml">
<!ENTITY rfc5246 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5246.xml">
<!ENTITY rfc5116 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5116.xml">
<!ENTITY rfc5288 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5288.xml">
<!ENTITY rfc5289 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5289.xml">
<!ENTITY rfc6347 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.6347.xml">
<!ENTITY tlschacha20poly1305 SYSTEM 
"http://xml.resource.org/public/rfc/bibxml3/reference.I-D.mavrogiannopoulos-chacha-tls.xml">
<!ENTITY tlscurve25519 SYSTEM 
"http://xml.resource.org/public/rfc/bibxml3/reference.I-D.josefsson-tls-curve25519.xml">
<!ENTITY eddsaed25519 SYSTEM 
"http://xml.resource.org/public/rfc/bibxml3/reference.I-D.josefsson-eddsa-ed25519.xml">
]>

<?rfc symrefs="yes"?>

<rfc category="std"
     ipr="trust200902"
     docName="draft-josefsson-tls-eddsa-02" >
     
  <front>
    
    <title abbrev="EdDSA and Ed25519 for TLS">
      EdDSA and Ed25519 for Transport Layer Security (TLS)
    </title>

    <author fullname="Simon Josefsson" initials="S." surname="Josefsson">
      <organization>SJD AB</organization>
      <address>
        <email>simon@josefsson.org</email>
      </address>
    </author>

    <date month="June" year="2015" />

    <keyword>TLS, Elliptic Curve Cryptography, EdDSA, Ed25519,
    Curve25519, X25519</keyword>

    <abstract>

      <t>This document introduce the public-key signature algorithm
      EdDSA for use in Transport Layer Security (TLS).  With the
      previous NamedCurve and ECPointFormat assignments for the
      Curve25519 ECDHE key exchange mechanism, this enables use of
      Ed25519 in TLS.  New Cipher Suites for EdDSA together with
      AES-GCM and ChaCha20-Poly1305 are introduced here.  This is
      intended to work with any version of TLS and Datagram TLS.</t>
      
    </abstract>

  </front>

  <middle>

    <section title="Introduction">

      <t><xref target="RFC5246">TLS</xref> and <xref
      target="RFC6347">DTLS</xref> support different key exchange
      algorithms and authentication mechanisms.  In <xref
      target="RFC4492">ECC in TLS</xref>, key exchange and
      authentication using ECC is specified, where the NamedCurve and
      ECPointFormat registries and associated TLS extensions are
      introduced.</t>

      <t>In <xref target="I-D.josefsson-tls-curve25519" /> support for
      ECDHE key exchange with the Curve25519 curve is added.  That
      document introduces a new NamedCurve value for Curve25519, and a
      new ECPointFormat value to correspond to the public-key
      encoding.</t>

      <t>This document describes how to use <xref
      target="I-D.josefsson-eddsa-ed25519">EdDSA and Ed25519</xref> as
      a new authentication mechanism in TLS, reusing the NamedCurve
      and ECPointFormat values already introduced for Curve25519, and
      finally specifying new Cipher Suites for Ed25519 with <xref
      target="RFC5288">AES-GCM</xref> and <xref
      target="I-D.mavrogiannopoulos-chacha-tls">ChaCha20-Poly1305</xref>.</t>

      <t>This document is a self-contained alternative to
      draft-josefsson-tls-eddsa2.  This document specify new cipher
      suites for EdDSA, whereas draft-josefsson-tls-eddsa2 reuse the
      ECDSA cipher suites for EdDSA.  It is an open issue which
      approach is to be prefered.</t>

      <section title="Requirements Terminology">

        <t>The key words "MUST", "MUST NOT", "REQUIRED", "SHALL",
        "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and
        "OPTIONAL" in this document are to be interpreted as described
        in <xref target="RFC2119" />.</t>

      </section>
      
    </section>

    <section title="The ECDHE_EDDSA Key Exchange Algorithm"
             anchor="SignatureAlgorithm" >
  
      <t>Negotiation of the authentication mechanism is signalled by
      sending a SignatureAlgorithm value.  Here we extend this
      enumeration for EdDSA.</t>

      <figure>
        <artwork><![CDATA[
   enum {
      eddsa(4)
   } SignatureAlgorithm;
]]></artwork>
      </figure>

      <t>EdDSA is suitable for use with TLS <xref target='RFC5246'/>
      and DTLS <xref target='RFC6347'/>.</t>

      <t>The new key exchange mechanism ECDHE_EDDSA provides forward
      secrecy.  The key exchange mechanism works just like ECDHE_ECDSA
      but with ECDSA replaced with EDDSA.  Currently the only
      applicable curve is Curve25519.</t>

      <t>The HashAlgorithm value to specify for EdDSA MUST be "none"
      as the EdDSA signature algorithm does not hash the input before
      signing.</t>
      
    </section>

    <section title="Cipher Suites"
             anchor="CipherSuites" >
  
      <t>The following Cipher Suite values are registered, using the
      ChaCha20/Poly1305 authenticated encryption with additional data (AEAD) cipher described in <xref
      target="I-D.mavrogiannopoulos-chacha-tls" /> and the AES Galois
      Counter Mode (GCM) cipher. The AES-GCM cipher suites use
      the AEAD algorithms
      AEAD_AES_128_GCM and AEAD_AES_256_GCM described in <xref
      target="RFC5116" />.  GCM is used as described in <xref
      target="RFC5288" />, but see also <xref target="RFC5289" />.</t>

      <figure>
        <artwork><![CDATA[

CipherSuite TLS_ECDHE_EDDSA_WITH_CHACHA20_POLY1305  = { 0xCC, 0xB0 }
CipherSuite TLS_ECDHE_EDDSA_WITH_AES_128_GCM_SHA256 = { 0xCC, 0xB1 }
CipherSuite TLS_ECDHE_EDDSA_WITH_AES_256_GCM_SHA384 = { 0xCC, 0xB2 }

]]></artwork>
      </figure>

      <t>The cipher suites are suitable for TLS <xref target='RFC5246'/> and DTLS <xref
      target='RFC6347'/>.</t>

    </section>
    
    <section title="IANA Considerations">

      <t>EdDSA should be registered in the Transport Layer Security
      (TLS) Parameters <xref target='IANA-TLS' /> registry under
      "SignatureAlgorithm" as follows.</t>

     <texttable>
       <preamble></preamble>
       <ttcol align='center'>Value</ttcol>
       <ttcol align='center'>Description</ttcol>
       <ttcol align='center'>DTLS-OK</ttcol>
       <ttcol align='center'>Reference</ttcol>
       <c>4</c>
       <c>eddsa</c>
       <c>Y</c>
       <c>This doc</c>
       <postamble></postamble>
     </texttable>

     <t>The follow cipher suites should be registered in the TLS
     Parameters registry under "TLS Cipher Suite Registry" as follows.
     They should all be marked as DTLS-OK.</t>

     <figure>
        <artwork><![CDATA[
CipherSuite TLS_ECDHE_EDDSA_WITH_CHACHA20_POLY1305  = { 0xCC, 0xB0 }
CipherSuite TLS_ECDHE_EDDSA_WITH_AES_128_GCM_SHA256 = { 0xCC, 0xB1 }
CipherSuite TLS_ECDHE_EDDSA_WITH_AES_256_GCM_SHA384 = { 0xCC, 0xB2 }
]]></artwork>
      </figure>

   </section>

   <section title="Security Considerations">

     <t>The security considerations of <xref
     target="RFC5246">TLS</xref>, <xref target="RFC6347">DTLS</xref>,
     <xref target="RFC4492">ECC in TLS</xref> <xref
     target="I-D.josefsson-tls-curve25519">Curve25519 in TLS</xref>,
     <xref target="I-D.josefsson-eddsa-ed25519">EdDSA and
     Ed25519</xref>, <xref
     target="I-D.mavrogiannopoulos-chacha-tls">ChaCha20-Poly1305</xref>,
     <xref target="RFC5116">AES-GCM</xref> an <xref
     target="RFC5288">AES-GCM in TLS</xref> are inherited.</t>

     <t>As with all cryptographic algorithms, the reader should stay
     informed about new research insights into the security of the
     algorithms involved.</t>

     <t>While discussed in the EdDSA/Ed25519 specification and papers,
     we would like to stress the significance of secure implementation
     of EdDSA/Ed25519.  For example, implementations ought to be
     constant-time to avoid certain attacks.</t>
     
   </section>

   <section title="Acknowledgements">

      <t>Thanks to Klaus Hartke and Nicolas Williams for fixes to the
      document.</t>

   </section>

  </middle>

  <back>

    <references title="Normative References">

      &rfc2119;
      &rfc4492;
      &rfc5116;
      &rfc5246;
      &rfc5288;
      &rfc6347;
      &tlscurve25519;
      &eddsaed25519;
      &tlschacha20poly1305;
      
    </references>

    <references title="Informative References">

      &rfc5289;

      <reference anchor="IANA-TLS" target="http://www.iana.org/assignments/tls-parameters/tls-parameters.xml">
        <front>
          <title>Transport Layer Security (TLS) Parameters</title>
          <author>
            <organization>Internet Assigned Numbers Authority</organization>
          </author>
          <date month="" year=""/>
        </front>
      </reference>

    </references>

  </back>
</rfc>
